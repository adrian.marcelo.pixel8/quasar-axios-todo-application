# ABOUT THIS PROJECT

This is the fourth project during our internship. A simple CRUD application that covers the basics of Vue.js and Axios. Topics covered are Composition API, Axios, Basic HTTP Request such as GET, POST, DELETE, and PUT, and also how Vue.js handles state changes such as using watch() to check if a certain property changes.

## ADDING TASK

![ADD](https://user-images.githubusercontent.com/63532775/219556654-95b51512-d256-4c09-968a-1b528f7b2be3.gif)

## UPDATING TASK

![UPDATE](https://user-images.githubusercontent.com/63532775/219556729-a766c0b7-8f52-4c79-b784-d561450a3a48.gif)

## DELETING TASK

![DELETE](https://user-images.githubusercontent.com/63532775/219556776-069d0079-1b14-4ee3-90d5-a30e6d16d487.gif)

## MARK AS DONE

![MARK AS DONE](https://user-images.githubusercontent.com/63532775/219556807-c0de86a3-707f-4e33-b99f-b513c81f05d2.gif)

### What I Learned from this project?

- How to use Axios to fetch data from a certain API
- How to use composition API
- How to use the QTable and modifying the value of field based on a certain value/condition
